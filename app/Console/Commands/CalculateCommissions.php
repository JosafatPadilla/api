<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Client;
use App\Commission;
use App\CommissionService;

class CalculateCommissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'commission:calculate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate the commission for the sales agent';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * Obtiene todos los Sales agents y calcula las comisiones del mes en curso.
     *
     * @return void
     */
    public function handle()
    {
        $salesAgents = Client::where('sales_agent', '=', '1')
                                //->where('id_clients', '=', '18')
                                //->limit(1)
                                ->get();
        $return = [];
        foreach ($salesAgents as $agent) {
            $id    = $agent->id_clients;
            $main  = Client::where('id_clients', $id)->first();
            $cs    = new CommissionService();
            $totals = $cs->calculateCommissions($main);
            $return[$id] = $totals;

            $declined = [];
            foreach ($totals as $key => $value) {
                if ($key != 'isActive') {
                    $declined[$key] = $value['declined'];
                }
            }

            $comission = new Commission;
            $comission->id_clients    = $id;
            $comission->amount        = $totals['level_1']['comission']+$totals['level_2']['comission']+$totals['level_3']['comission']+$totals['level_4']['comission'];
            $comission->calculated_at = Carbon::now();
            $comission->is_active     = $totals['isActive'];
            $comission->level_1       = (isset($totals['level_1']['count']) ? $totals['level_1']['count']:0);
            $comission->level_2       = (isset($totals['level_2']['count']) ? $totals['level_2']['count']:0);
            $comission->level_3       = (isset($totals['level_3']['count']) ? $totals['level_3']['count']:0);
            $comission->level_4       = (isset($totals['level_4']['count']) ? $totals['level_4']['count']:0);
            $comission->declined      = $declined;
            //$comission->save();

            $this->info('Cliente: ('.$agent->name.')');
            $this->info('   total: '.$totals['level_1']['count']);
            $this->info('        Level 1: '.$totals['level_1']['count']);
            $this->info('        Level 2: '.$totals['level_2']['count']);
            $this->info('        Level 3: '.$totals['level_3']['count']);
            $this->info('        Level 4: '.$totals['level_4']['count']);
            $this->error('   Declinados: '.json_encode($declined));
        }
    }
}
