<?php

namespace App\Http\Controllers;

use App\Notification;
use App\Http\Controllers\Main;
use Request;

class NotificationsController extends Main {

    /**
     * index
     * Devuelve todas las notificaciones (\App\Notification) después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @App\Notification
     * 
     * @return response NULL|Error(404)
     */
    public function index() {

        try {

            $notifications = Notification::query();

            foreach(Request::query() as $name => $value) {

                $notifications = $notifications->where($name, $value)->orderBy('created_at', 'desc');

            }

            return Main::response(true, 'OK', $notifications->get());

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);

        }

    }

}
