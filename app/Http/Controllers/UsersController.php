<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use App\Client;
use App\Role;
use App\Group;
use App\Contract;
use App\Device;
use Illuminate\Cache\RateLimiter;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Mail\Message;
use Illuminate\Http\Request;
use Validator;
use DB;
use Conekta;
use Conekta_Customer;
use Facebook\Facebook;
use Mail;

class UsersController extends Main {

    use ThrottlesLogins;

    public function __construct() {

        parent::__construct(['postLogin']);

    }
    /**
     * index
     * Devuelve todas los usuarios (\App\Users) después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @param  Request $request 
     * @return response         OK|Internal Server Error(500)
     */
    public function index(Request $request) {

        try {

            $users = DB::table('users');

            foreach($request->query() as $name => $value) {

                switch($name) {

                    case 'name':
                        $users = $users->where($name, 'LIKE', "%$value%");
                    break;

                    case 'role':
                        $role = DB::table('roles')
                            ->where('name', $value)
                            ->first();
                        $users = DB::table('users')
                            ->join('role_user', 'users.id', '=', 'role_user.user_id')
                            ->select('users.id', 'users.name', 'users.email', 'users.created_at', 'users.updated_at', 'users.image', 'users.type', 'users.phone', 'users.contractor')
                            ->where('role_id', $role ? $role->id : null)
                            ->where('status',"1");
                    break;

                    default:
                        $users = $users->where($name, $value);
                    break;
                }

            }

            $users = $users->get();

            foreach($users as &$user) {

                $user = $this->resolveRelations($user);

            }

            return Main::response(true, 'OK', $users);

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);

        }

    }

    /**
     * getToken
     * Genera un TOKEN para que se puedan comunicar
     * 
     * @return response     Token
     */
    public function getToken() {

        return response()->json(['token' => csrf_token()]);

    }

    /**
     * getLogout
     *
     * @return void
     */
    public function getLogout() {

        Auth::logout();

    }

    /**
     * loginUsername
     *
     * @return void
     */
    public function loginUsername() {

        return 'email';

    }

    /**
     * postLogin
     *
     * @param Request $request
     * @return void
     */
    public function postLogin(Request $request) {

        $validator = Validator::make(
            $request->all(),
            [
                'email'    => 'required|email|max:255',
                'password' => 'required|min:6|max:60'
            ]
        );

        #file_put_contents('log', json_encode($request->all()) . " " . date('Y-m-d H:i:s') . "\n", FILE_APPEND);

        if($validator->fails()) {

            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

        } else if($this->hasTooManyLoginAttempts($request)) {

            $seconds = app(RateLimiter::class)->availableIn($this->getThrottleKey($request));
            
            return response("Too many login attempts. Please try again in $seconds seconds.", 401);

        } else if(Auth::attempt($request->only('email', 'password'), $request->has('remember'))) {
            
            //if(!isset($input['appVersion'])) return response('WAAAAAAA', 401);

            $user = Auth::user();

            $this->clearLoginAttempts($request);

            return response()->json(
                [
                    'user' => $this->resolveRelations($user)
                ]
            );

        } else {
            dd('qwe');
            $this->incrementLoginAttempts($request);

            return response(null, 401);

        }

    }

    /**
     * postRegister
     *
     * @param Request $request
     * @return void
     */
    public function postRegister(Request $request) {

        $validator = Validator::make(
            $input = $request->all(),
            [
                'name'     => 'required|max:255',
                'email'    => 'required|unique:users|email|max:255',
                'password' => 'required|min:6|max:60',
                'role'     => 'string',
                'phone'    => 'string',
                'image'    => 'image|min:1|max:10240',
                'contractor' => 'string'
            ]
        );

        if($validator->fails()) {

            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

        }

        $user = new User;
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->phone = @ $input['phone'];
        $user->contractor = @ $input['contractor'];
        $user->password = bcrypt($input['password']);
        $user->status=1;

        if(isset($input['image'])) {
            $user->type = $input['image']->getMimeType();
            $user->image = base64_encode(file_get_contents($input['image']->getRealPath()));
        }

        $user->save();

        if(isset($input['role']) && ($role = Role::where('name', '=', $input['role'])->first())) {

            $user->attachRole($role);

        } else {

            $role = Role::where('name', '=', 'Administrator')->first();

            if($role) {

                $user->attachRole($role);

            }

        }

        $user->roles = $user->roles()->get();

        return response()->json(['user' => $user]);

    }

    public function show($id) {

        if($user = User::find($id)) {

            return Main::response(true, 'OK', $this->resolveRelations($user));

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

    //function to active an unactive users
    /**
     * toogleStatus
     *
     * @param mixed $id
     * @return void
     */
    public function toogleStatus($id){
             if($user = User::find($id)){
                
                $user->status=!$user->status;
                $user->save();
                // print_r($user);
               return Main::response(true, 'OK', $this->resolveRelations($user));
             }else{
                return Main::response(false, 'Not Found', null, 404);
                //print_r($user);
            }
    }

    /**
     * update
     *
     * @param mixed $id
     * @param Request $request
     * @return void
     */
    public function update($id, Request $request) {

        if($user = User::find($id)) {

            try {

                $input = $request->all();

                $validator = Validator::make(
                    $input,
                    [
                        'name'           => 'string',
                        'password'       => 'string|min:6',
                        'phone'          => 'string',
                        'image'          => 'image|min:1|max:10240',
                        'contractor'     => 'string'
                    ]
                );

                if($validator->fails()) {

                    return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

                }

                if(isset($input['name'])) {

                    $user->name = $input['name'];
                    $client = Client::where('id_users', '=', $user->id)->first();
                    if($client) {
                        $client->name = $input['name'];
                        $client->save();
                    }

                }

                if(isset($input['password'])) {

                    $user->password = bcrypt($input['password']);

                }

                if(isset($input['phone'])) {

                    $user->phone = $input['phone'];

                }

                if(isset($input['image'])) {

                    $user->type = $input['image']->getMimeType();
                    $user->image = base64_encode(file_get_contents($input['image']->getRealPath()));

                }

                if(isset($input['contractor'])) {

                    $user->contractor = $input['contractor'];

                }

                if(isset($input['email'])) {

                    $user->email = $input['email'];
                    $client = Client::where('id_users', '=', $user->id)->first();
                    if($client) {
                        $client->email = $input['email'];
                        $client->save();
                    }

                }

                $user->save();

                return Main::response(true, 'OK', $this->resolveRelations($user));

            } catch(\Exception $e) {

                return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);

            }

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

    /**
     * resolveRelations
     *
     * @param mixed $user
     * @return void
     */
    private function resolveRelations($user) {

        $roles = DB::table('role_user')
            ->where('user_id', $user->id)
            ->get();

        foreach($roles as &$rol) {

            $rol = Role::find($rol->role_id);

        }

        $user->roles = $roles;

        return $user;

    }

    /**
     * postEmail
     *
     * @param Request $request
     * @return void
     */
    public function postEmail(Request $request) {

        $input = $request->all();

        $validator = Validator::make(
            $input,
            [
                'email' => 'required|email'
            ]
        );

        if($validator->fails()) {

            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

        }

        $response = Password::sendResetLink(
            $request->only('email'),
            function(Message $message) {
                $message->subject('Reestablecer contraseña');
            }
        );

        switch($response) {
            case Password::RESET_LINK_SENT:
                return Main::response(true, 'OK', null);
            case Password::INVALID_USER:
                return Main::response(false, 'Not Found', null, 404);
        }

    }

    /**
     * postReset
     *
     * @param Request $request
     * @return void
     */
    public function postReset(Request $request) {

        $input = $request->all();

        $validator = Validator::make(
            $input,
            [
                'token' => 'required',
                'email' => 'required|email',
                'password' => 'required|min:6'
            ]
        );

        if($validator->fails()) {

            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

        }

        $_user = null;

        $response = Password::reset(
            $request->only('email', 'password', 'token', 'password_confirmation'),
            function ($user, $password) use(&$_user) {
                $_user = $user;
                $user->password = bcrypt($password);
                $user->save();
                Auth::login($user);
            }
        );

        switch($response) {
            case Password::PASSWORD_RESET:
                return Main::response(true, 'OK', $this->resolveRelations($_user));
            default:
                return Main::response(false, 'Internal Server Error', trans($response), 500);
        }
    }

    /**
     * template
     *
     * @param mixed $id
     * @return void
     */
    public function template($id = null) {

        $url = env('URL_API') . '/auth/facebook' . ($id ? "?seller=$id" : '');

        return '<html>
            <head>
            	<title>Aguagente</title>
            	<meta property="og:type"          content="website" />
            	<meta property="og:title"         content="Regístrate en Aguagente" />
            	<meta property="og:description"   content="Entra y registráte para conocer el mundo de Aguagente." />
            	<meta property="og:image"         content="http://panel.aguagente.com/registrarme.png" />
            </head>
            <body>
                <script>
                    window.location = "'.$url.'";
                </script>
            </body>
        </html>';

    }

    /**
     * getRegisterFacebook
     *
     * @param Request $request
     * @return void
     */
    public function getRegisterFacebook(Request $request) {

        $input = $request->all();

        $fb = new Facebook([
            'app_id' => env('FACEBOOK_APP_ID', '1849235075335660'),
            'app_secret' => env('FACEBOOK_APP_SECRET', 'ff97d982f3d67de08c997a714378c8fd'),
            'default_graph_version' => 'v2.8'
        ]);

        $helper = $fb->getRedirectLoginHelper();
        $permissions = ['email'];

        return Main::response(
            true,
            'Found',
            null,
            302,
            [
                'Location' => $helper->getLoginUrl(env('URL_API') . '/auth/facebook-register'.(isset($input['seller']) ? "?seller={$input['seller']}" : ''), $permissions)
            ]
        );

    }

    /**
     * registerFacebook
     *
     * @param Request $request
     * @return void
     */
    public function registerFacebook(Request $request) {

        try {

            $fb = new Facebook([
                'app_id' => env('FACEBOOK_APP_ID', '1849235075335660'),
                'app_secret' => env('FACEBOOK_APP_SECRET', 'ff97d982f3d67de08c997a714378c8fd'),
                'default_graph_version' => 'v2.8'
            ]);

            $helper = $fb->getRedirectLoginHelper();
            $fb->setDefaultAccessToken($helper->getAccessToken());

            $response = $fb->get('/me?fields=first_name,last_name,email,picture.width(400).height(400)');
            $userNode = $response->getGraphUser();

            $password = str_random(10);

            DB::beginTransaction();

            if(User::where('email', '=', $userNode->getEmail())->first())
                return Main::response(true, 'Found', null, 302, ['Location'=>env('URL_MICUENTA').'/#/login?error=Usuario ha sido registrado']);

            $user = new User();
            $user->name = $userNode->getFirstName() . " " . $userNode->getLastName();
            $user->email = $userNode->getEmail();
            $user->password = bcrypt($password);
            $user->save();
            $user->attachRole(
                Role::where('name', '=', 'Client')->first()
            );

            if(isset($input['seller'])) {

                $referrer = Client::where('id_clients', '=', $input['seller'])->first();
                $referrerGroup = Group::find($referrer->id_groups);
                $signIntoGroup = Group::find($referrerGroup->sign_into);

            } else {

                $referrerGroup = Group::find(2);
                $signIntoGroup = Group::find($referrerGroup->sign_into);

            }

            $client = new Client();
            $client->id_users           = $user->id;
            $client->id_groups          = $signIntoGroup->id_groups;
            $client->name               = $user->name;
            $client->email              = $user->email;
            $client->signed_in_id       = $signIntoGroup->id_groups;
            $client->signed_in          = $signIntoGroup->name;
            $client->deposit            = $signIntoGroup->deposit;
            $client->installation_fee   = $signIntoGroup->installation_fee;
            $client->monthly_fee        = $signIntoGroup->monthly_fee;
            $client->referred_by        = @ $referrer->id_clients;
            $client->status             = 'invalid';
            $client->save();

            Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
            Conekta::setLocale('es');

            $customer = Conekta_Customer::create(
                array(
                    'name'  => $client->name,
                    'email' => $client->email
                )
            );

            $client->conekta_token = $customer->id;
            $client->save();

            $contract = new Contract();
            $contract->id_clients = $client->id_clients;
            $contract->save();

            Mail::send(
                'email.passwordNotification',
                array(
                    'name'     => $client->name,
                    'username' => $client->email,
                    'password' => $password
                ),
                function($message) use ($client) {
                    $message
                        ->to($client->email, $client->name)
                        ->subject(
                            'Bienvenido a Aguagente'
                        );
                }
            );

            DB::commit();

            Auth::login($user);

            return Main::response(true, 'Found', null, 302, ['Location'=>env('URL_MICUENTA').'/#/login?success=Usuario registrado correctamente']);

        } catch(\Exception $e) {

            DB::rollback();

            return Main::response(false, 'Internal Server Error', null, 500);

        }

    }

    /**
     * loginFacebook
     *
     * @param Request $request
     * @return void
     */
    public function loginFacebook(Request $request) {

        $input = $request->all();

        $validator = Validator::make(
            $input,
            [
                'token' => 'required|string'
            ]
        );

        if($validator->fails()) {

            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

        }

        $fb = new Facebook([
            'app_id' => env('FACEBOOK_APP_ID', '1849235075335660'),
            'app_secret' => env('FACEBOOK_APP_SECRET', 'ff97d982f3d67de08c997a714378c8fd'),
            'default_graph_version' => 'v2.8'
        ]);

        try {

            $response = $fb->get('/me?fields=email', $input['token']);
            $userNode = $response->getGraphUser();

            $user = User::where('email', '=', $userNode->getEmail())->first();

            Auth::login($user);

            return response()->json(
                [
                    'user' => $this->resolveRelations($user)
                ]
            );

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', $e->getMessage(), 500);

        }

    }

    /**
     * registerDevice
     *
     * @param Request $request
     * @return void
     */
    public function registerDevice(Request $request) {

        $input = $request->all();

        $validator = Validator::make(
            $input,
            [
                'registration_id' => 'required|string',
                'platform' => 'required|in:ios,android'
            ]
        );

        if($validator->fails()) {

            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

        }

        $user = Auth::user();

        $device = Device::where('id_users', '=', $user->id)
                        ->where('registration_id', '=', $input['registration_id'])
                        ->first();

        if(!$device) {

            $device = new Device;
            $device->registration_id = $input['registration_id'];
            $device->platform = $input['platform'];
            $device->id_users = $user->id;
            $device->save();

        }

        return Main::response(true, 'OK', $device, 200);

    }

}
