<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AnswerOption extends Model {

    protected $table = 'answers_options';
    protected $primaryKey = 'id_answers_options';

}

