<?php 

namespace Test;

use App\Level;
use App\LevelService;
use App\Client;
use App\IClientRepository;
use App\ClientService;
use App\CommissionService;
use App\MySQLClientRepository;

class MemoryClientRepository implements IClientRepository {

	private $clients = array();

	public function __construct(array $clients) {

		$this->clients = $clients;

	}

	/**
	 * @param  integer
	 * @return Client
	 */
	public function getById($id_clients) {

		$stack = array();

		array_push($stack, $this->clients[0]);

		while($stack) {

			$node = array_pop($stack);

			if($node->id_clients == $id_clients) {

				return $node;

			}

			foreach($node->clients as $client) {
				
				array_push($stack, $client);

			}

		}

		return null;

	}

	/**
	 * @param  integer $id_clients
	 * @return Client[]
	 */
	public function getPayingClients($id_clients) {

		$stack = array();

		array_push($stack, $this->clients[0]); // root

		while($stack) {

			$node = array_pop($stack);

			if($node->id_clients == $id_clients) {

				return array_filter(
					$node->clients,
					function($c) {
						return $c->isPayingClient;
					}
				);

			}

			foreach($node->clients as $client) {
				
				array_push($stack, $client);

			}

		}

		return null;

	}

	/**
	 * @param  integer $id_clients
	 * @return integer[]
	 */
	public function getSignedClientsTree($id_clients) {

	}

	/**
	 * @param  integer $id_clients
	 * @param  integer $months
	 * @return integer
	 */
	public function getSignedClientsFromThePastMonths($id_clients, $months) {

	}

	/**
	 * @param  integer $id_clients
	 * @return float
	 */
	public function getCommissionsFromPastMonth($id_clients) {

	}

	/**
	 * @param  Client $client
	 */
	public function save(Client $client) {

	}

}

class ComissionTest extends \PHPUnit_Framework_TestCase {

	private function client($id_clients, $referred_by, $isPayingClient, $actualLevel, $clients) {

		$client = new Client();
		$client->id_clients = $id_clients;
		$client->referred_by = $referred_by;
		$client->isPayingClient = $isPayingClient;
		$client->level = $actualLevel;
		$client->clients = $clients;
		return $client;

	}

	/**
	 * @test
	 */
	public function setLevels_lessThan6clients_VIAJERO() {

		$clients = array(
			$this->client(
				1,
				NULL,
				true,
				Level::VIAJERO,
				array()
			)
		);

		$clientRepository = new MemoryClientRepository($clients);

		(new LevelService($clientRepository))->setLevels($clientRepository->getById(1));
		
		$this->assertEquals($clientRepository->getById(1)->level, Level::VIAJERO);

	}

	/**
	 * @test
	 */
	public function setLevels_6clientsLessThan6paying_VIAJERO() {

		$clients = array(
			$this->client(
				1,
				NULL,
				true,
				Level::VIAJERO,
				array(
					$this->client( 2, 1, true, Level::VIAJERO, array()),
					$this->client( 3, 1, true, Level::VIAJERO, array()),
					$this->client( 4, 1, true, Level::VIAJERO, array()),
					$this->client( 5, 1, true, Level::VIAJERO, array()),
					$this->client( 6, 1, false, Level::VIAJERO, array()),
					$this->client( 7, 1, false, Level::VIAJERO, array())
				)
			)
		);

		$clientRepository = new MemoryClientRepository($clients);

		(new LevelService($clientRepository))->setLevels($clientRepository->getById(7));
		
		$this->assertEquals($clientRepository->getById(1)->level, Level::VIAJERO);

	}

	/**
	 * @test
	 */
	public function setLevels_6clients6paying_MARINERO() {

		$clients = array(
			$this->client(
				1,
				NULL,
				true,
				Level::VIAJERO,
				array(
					$this->client( 2, 1, true, Level::VIAJERO, array()),
					$this->client( 3, 1, true, Level::VIAJERO, array()),
					$this->client( 4, 1, true, Level::VIAJERO, array()),
					$this->client( 5, 1, true, Level::VIAJERO, array()),
					$this->client( 6, 1, true, Level::VIAJERO, array()),
					$this->client( 7, 1, true, Level::VIAJERO, array())
				)
			)
		);

		$clientRepository = new MemoryClientRepository($clients);

		(new LevelService($clientRepository))->setLevels($clientRepository->getById(7));
		
		$this->assertEquals($clientRepository->getById(1)->level, Level::MARINERO);

	}

	/**
	 * @test
	 */
	public function setLevels_16clientsLessThan16paying_MARINERO() {

		$clients = array(
			$this->client(
				1,
				NULL,
				true,
				Level::VIAJERO,
				array(
					$this->client( 2, 1, true, Level::VIAJERO, array()),
					$this->client( 3, 1, true, Level::VIAJERO, array()),
					$this->client( 4, 1, true, Level::VIAJERO, array()),
					$this->client( 5, 1, true, Level::VIAJERO, array()),
					$this->client( 6, 1, true, Level::VIAJERO, array()),
					$this->client( 7, 1, true, Level::VIAJERO, array()),
					$this->client( 8, 1, true, Level::VIAJERO, array()),
					$this->client( 9, 1, true, Level::VIAJERO, array()),
					$this->client(10, 1, true, Level::VIAJERO, array()),
					$this->client(11, 1, true, Level::VIAJERO, array()),
					$this->client(12, 1, true, Level::VIAJERO, array()),
					$this->client(13, 1, true, Level::VIAJERO, array()),
					$this->client(14, 1, true, Level::VIAJERO, array()),
					$this->client(15, 1, true, Level::VIAJERO, array()),
					$this->client(16, 1, true, Level::VIAJERO, array()),
					$this->client(17, 1, false, Level::VIAJERO, array())
				)
			)
		);

		$clientRepository = new MemoryClientRepository($clients);

		(new LevelService($clientRepository))->setLevels($clientRepository->getById(17));
		
		$this->assertEquals($clientRepository->getById(1)->level, Level::MARINERO);

	}

	/**
	 * @test
	 */
	public function setLevels_16clients16paying_CABO() {

		$clients = array(
			$this->client(
				1,
				NULL,
				true,
				Level::VIAJERO,
				array(
					$this->client( 2, 1, true, Level::VIAJERO, array()),
					$this->client( 3, 1, true, Level::VIAJERO, array()),
					$this->client( 4, 1, true, Level::VIAJERO, array()),
					$this->client( 5, 1, true, Level::VIAJERO, array()),
					$this->client( 6, 1, true, Level::VIAJERO, array()),
					$this->client( 7, 1, true, Level::VIAJERO, array()),
					$this->client( 8, 1, true, Level::VIAJERO, array()),
					$this->client( 9, 1, true, Level::VIAJERO, array()),
					$this->client(10, 1, true, Level::VIAJERO, array()),
					$this->client(11, 1, true, Level::VIAJERO, array()),
					$this->client(12, 1, true, Level::VIAJERO, array()),
					$this->client(13, 1, true, Level::VIAJERO, array()),
					$this->client(14, 1, true, Level::VIAJERO, array()),
					$this->client(15, 1, true, Level::VIAJERO, array()),
					$this->client(16, 1, true, Level::VIAJERO, array()),
					$this->client(17, 1, true, Level::VIAJERO, array())
				)
			)
		);

		$clientRepository = new MemoryClientRepository($clients);

		(new LevelService($clientRepository))->setLevels($clientRepository->getById(17));
		
		$this->assertEquals($clientRepository->getById(1)->level, Level::CABO);

	}

	/**
	 * @test
	 */
	public function setLevels_16clientsLessThan10freeWater_CABO() {

		$clients = array(
			$this->client(
				1,
				NULL,
				true,
				Level::VIAJERO,
				array(
					$this->client( 2, 1, true, Level::MARINERO, array()),
					$this->client( 3, 1, true, Level::MARINERO, array()),
					$this->client( 4, 1, true, Level::MARINERO, array()),
					$this->client( 5, 1, true, Level::MARINERO, array()),
					$this->client( 6, 1, true, Level::MARINERO, array()),
					$this->client( 7, 1, true, Level::MARINERO, array()),
					$this->client( 8, 1, true, Level::MARINERO, array()),
					$this->client( 9, 1, true, Level::MARINERO, array()),
					$this->client(10, 1, true, Level::MARINERO, array()),
					$this->client(11, 1, true, Level::VIAJERO, array()),
					$this->client(12, 1, true, Level::VIAJERO, array()),
					$this->client(13, 1, true, Level::VIAJERO, array()),
					$this->client(14, 1, true, Level::VIAJERO, array()),
					$this->client(15, 1, true, Level::VIAJERO, array()),
					$this->client(16, 1, true, Level::VIAJERO, array()),
					$this->client(17, 1, true, Level::VIAJERO, array())
				)
			)
		);

		$clientRepository = new MemoryClientRepository($clients);

		(new LevelService($clientRepository))->setLevels($clientRepository->getById(17));
		
		$this->assertEquals($clientRepository->getById(1)->level, Level::CABO);

	}

	/**
	 * @test
	 */
	public function setLevels_16clientsMoreThan9LessThan15freeWater_CAPITAN() {

		$clients = array(
			$this->client(
				1,
				NULL,
				true,
				Level::VIAJERO,
				array(
					$this->client( 2, 1, true, Level::MARINERO, array()),
					$this->client( 3, 1, true, Level::MARINERO, array()),
					$this->client( 4, 1, true, Level::MARINERO, array()),
					$this->client( 5, 1, true, Level::MARINERO, array()),
					$this->client( 6, 1, true, Level::MARINERO, array()),
					$this->client( 7, 1, true, Level::MARINERO, array()),
					$this->client( 8, 1, true, Level::MARINERO, array()),
					$this->client( 9, 1, true, Level::MARINERO, array()),
					$this->client(10, 1, true, Level::MARINERO, array()),
					$this->client(11, 1, true, Level::MARINERO, array()),
					$this->client(12, 1, true, Level::MARINERO, array()),
					$this->client(13, 1, true, Level::MARINERO, array()),
					$this->client(14, 1, true, Level::MARINERO, array()),
					$this->client(15, 1, true, Level::MARINERO, array()),
					$this->client(16, 1, true, Level::VIAJERO, array()),
					$this->client(17, 1, true, Level::VIAJERO, array())
				)
			)
		);

		$clientRepository = new MemoryClientRepository($clients);

		(new LevelService($clientRepository))->setLevels($clientRepository->getById(17));
		
		$this->assertEquals($clientRepository->getById(1)->level, Level::CAPITAN);

	}

	/**
	 * @test
	 */
	public function setLevels_16clientsMoreThan15freeWater_ALMIRANTE() {

		$clients = array(
			$this->client(
				1,
				NULL,
				true,
				Level::VIAJERO,
				array(
					$this->client( 2, 1, true, Level::MARINERO, array()),
					$this->client( 3, 1, true, Level::MARINERO, array()),
					$this->client( 4, 1, true, Level::MARINERO, array()),
					$this->client( 5, 1, true, Level::MARINERO, array()),
					$this->client( 6, 1, true, Level::MARINERO, array()),
					$this->client( 7, 1, true, Level::MARINERO, array()),
					$this->client( 8, 1, true, Level::MARINERO, array()),
					$this->client( 9, 1, true, Level::MARINERO, array()),
					$this->client(10, 1, true, Level::MARINERO, array()),
					$this->client(11, 1, true, Level::MARINERO, array()),
					$this->client(12, 1, true, Level::MARINERO, array()),
					$this->client(13, 1, true, Level::MARINERO, array()),
					$this->client(14, 1, true, Level::MARINERO, array()),
					$this->client(15, 1, true, Level::MARINERO, array()),
					$this->client(16, 1, true, Level::MARINERO, array()),
					$this->client(17, 1, true, Level::VIAJERO, array())
				)
			)
		);

		$clientRepository = new MemoryClientRepository($clients);

		(new LevelService($clientRepository))->setLevels($clientRepository->getById(17));
		
		$this->assertEquals($clientRepository->getById(1)->level, Level::ALMIRANTE);

	}

	/**
	 * @test
	 */
	public function setLevels_() {

		$clients = array(
			$this->client(
				1,
				NULL,
				true,
				Level::CABO,
				array(
					$this->client( 2, 1, true, Level::MARINERO, array()),
					$this->client( 3, 1, true, Level::MARINERO, array()),
					$this->client( 4, 1, true, Level::MARINERO, array()),
					$this->client( 5, 1, true, Level::MARINERO, array()),
					$this->client( 6, 1, true, Level::MARINERO, array()),
					$this->client( 7, 1, true, Level::MARINERO, array()),
					$this->client( 8, 1, true, Level::MARINERO, array()),
					$this->client( 9, 1, true, Level::MARINERO, array()),
					$this->client(10, 1, true, Level::MARINERO, array()),
					$this->client(11, 1, true, Level::VIAJERO, array()),
					$this->client(12, 1, true, Level::VIAJERO, array()),
					$this->client(13, 1, true, Level::VIAJERO, array()),
					$this->client(14, 1, true, Level::VIAJERO, array()),
					$this->client(15, 1, true, Level::VIAJERO, array()),
					$this->client(16, 1, true, Level::VIAJERO, array()),
					$this->client(
						17,
						1,
						true,
						Level::VIAJERO,
						array(
							$this->client(18, 17, true, Level::VIAJERO, array()),
							$this->client(19, 17, true, Level::VIAJERO, array()),
							$this->client(20, 17, true, Level::VIAJERO, array()),
							$this->client(21, 17, true, Level::VIAJERO, array()),
							$this->client(22, 17, true, Level::VIAJERO, array()),
							$this->client(23, 17, true, Level::VIAJERO, array())
						)
					)
				)
			)
		);

		$clientRepository = new MemoryClientRepository($clients);

		(new LevelService($clientRepository))->setLevels($clientRepository->getById(23));
		
		$this->assertEquals($clientRepository->getById(17)->level, Level::MARINERO);
		$this->assertEquals($clientRepository->getById(1)->level, Level::CAPITAN);

	}

	/**
	 * @test
	 */
	public function calculateCommisions() {

		$clientService = $this->getMockBuilder(ClientService::class)->disableOriginalConstructor()->getMock();
		$clientService
			->method('isActive')
			->willReturn(
				true
			);

		$clientRepository = $this->getMockBuilder(IClientRepository::class)->getMock();
		$clientRepository
			->method('getSignedClientsTree')
			->willReturn(
				[
					'level_1' => 10,
					'level_2' => 20,
					'level_3' => 30,
					'level_4' => 40,
				]
			);

		$client = new Client;
		$client->level = Level::MARINERO;

		$this->assertEquals(
			(new CommissionService(
				$clientRepository,
				$clientService
			))->calculateCommissions($client),
			[
				'is_active' => true,
				'itemization' => [
					'level_1' => 10,
					'level_2' => 20,
					'level_3' => 30,
					'level_4' => 40
				],
				'total' => 300
			]
		);

	}

	/**
	 * @test
	 */
	public function isActive() {

		$clientRepository = $this->getMockBuilder(IClientRepository::class)->getMock();
		$clientRepository
			->method('getCommissionsFromPastMonth')
			->willReturn(
				3000
			);

		$clientRepository
			->method('getSignedClientsFromThePastMonths')
			->willReturn(
				0
			);

		$this->assertEquals(
			(new ClientService($clientRepository))->isActive(1),
			true
		);

	}

	/**
	 * @test
	 */
	public function mysql() {

		$app = require __DIR__.'/../bootstrap/app.php';
        $app->make(\Illuminate\Contracts\Console\Kernel::class)->bootstrap();

		$clientRepository = new MySQLClientRepository();
		$clientRepository->getSignedClientsTree(1);
		$clientRepository->getById(1);
		$clientRepository->getSignedClientsFromThePastMonths(1, 1);
		$clientRepository->getCommissionsFromPastMonth(1);

	}

}
